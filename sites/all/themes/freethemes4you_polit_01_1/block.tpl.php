<div class="art-block clear-block block block-<?php print $block->module ?>" id="block-<?php print $block->module .'-'. $block->delta; ?>">
    <div class="art-block-body">

	<?php if (!empty($block->subject)): ?>
<div class="art-blockheader">
		    <h3 class="t subject">	
			<?php echo $block->subject; ?>
</h3>
		</div>
		    
	<?php endif; ?>
<div class="art-blockcontent content">
	    <div class="art-blockcontent-body">
	
		<?php echo $block->content; ?>

	
	    </div>
	</div>
	

    </div>
</div>
