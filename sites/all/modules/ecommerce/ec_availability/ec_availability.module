<?php

/**
 * @file
 * Track availability of products.
 */

/**
 * Implementation of hook_init().
 */
function ec_availability_init() {
  if (module_exists('ec_cart')) {
    $expires_period = 300;
    $items = array();
    $result = db_query("SELECT * FROM {ec_cart} c INNER JOIN {node} n ON c.nid = n.nid WHERE cookie_id = '%s'");
    while ($node = db_fetch_object($result)) {
      if (!empty($node->data) && ($extra = unserialize($node->data))) {
        foreach ($extra as $key => $value) {
          $node->{$key} = $value;
        }
      }
      $items[$node->nid] = $node;
    }
    if (!empty($items)) {
      $result = db_query("SELECT * FROM {ec_available_alloc} WHERE cookie_id = '%s' AND expires < %d", ec_checkout_get_id(), time());

      while ($row = db_fetch_object($result)) {
        if (ec_availability_ec_checkout_validate_item($items[$row->nid], 'cart', $items[$row->nid]->qty, NULL, NULL, EC_VALIDATE_ITEM_SEVERITY_LOW) !== FALSE) {
          db_query("UPDATE {ec_available_alloc} SET expires = %d WHERE nid = %d AND cookie_id = '%s'", time()+$expires_period, $row->nid, ec_checkout_get_id());
        }
        else {
          db_query("DELETE FROM {ec_available_alloc} WHERE nid = %d AND cookie_id = '%s'", $row->nid, ec_checkout_get_id());
          drupal_set_message(t('Temporary allocation for !title has expired, Please check availability to see if there is any left.', array('!title' => l($items[$row->nid]->title, 'node/'. $row->nid))), 'error');
        }
      }
    }
  }
}

/**
 * Implementation hook_exit().
 */
function ec_availability_exit() {
  $expires_period = 300;

  if (module_exists('ec_checkout')) {
    db_query("UPDATE {ec_available_alloc} SET expires = %d WHERE cookie_id = '%s'", time()+$expires_period, ec_checkout_get_id());
  }
}

/**
 * Implementation of hook_link().
 */
function ec_availability_link($type, $node = NULL, $teaser = FALSE) {
  $l = array();

  switch ($type) {
    case 'node':
      if (!variable_get('ec_product_cart_addition_by_link', TRUE) ||
        ($teaser && !variable_get('ec_product_cart_on_teaser', 1))) {
        break;
      }

    case 'checkout':
      if (isset($node->ptype) && ec_product_feature_exists($node, 'availability')) {
        $alloc = ec_availability_get_current($node->nid);
        if ($alloc['actual'] <= 0) {
          $l['no_stock'] = array(
            'title' => t('Out of stock'),
          );
        }
        elseif ($alloc['current'] <= 0) {
          $l['temp_no_stock'] = array(
            'title' => t('Temporarily out of stock'),
          );
        }
      }
      break;
  }

  return $l;
}

/**
 * Implementation of hook_nodeapi().
 */
function ec_availability_nodeapi($node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'view':
      if (isset($node->ptype) && ec_product_feature_exists($node, 'availability') && !variable_get('ec_product_cart_addition_by_link', 1) && (!$a3 || variable_get('ec_product_cart_on_teaser', 1))) {
        $alloc = ec_availability_get_current($node->nid);

        if ($alloc['actual'] <= 0) {
          $node->content['no_stock'] = array(
            '#value' => t('Out of stock'),
            '#prefix' => '<div class="ec-available-no-stock">',
            '#suffix' => '</div>',
            '#weight' => 5,
          );
        }
        elseif ($alloc['current'] <= 0) {
          $node->content['temp_no_stock'] = array(
            '#value' => t('Temporarily out of stock'),
            '#prefix' => '<div class="ec-available-no-stock ec-available-temporarily-no-stock">',
            '#suffix' => '</div>',
            '#weight' => 5,
          );
        }
      }
      break;
  }
}

/**
 * Implementation of hook_product_form_alter().
 */
function ec_availability_form_alter(&$form, &$form_state, $form_id) {
  if (ec_product_form_is_product($form_id, $form) && ec_product_feature_exists($form['#node']->ptype, 'availability')) {
    $node =& $form['#node'];
    $form['product']['inventory'] = array(
      '#type' => 'fieldset',
      '#title' => t('Availability control'),
    );
    $form['product']['inventory']['available'] = array(
      '#type' => 'textfield',
      '#title' => t('Items available'),
      '#default_value' => ((isset($node->available) && $node->available != '') ? $node->available : 0),
      '#size' => 25,
      '#maxlength' => 50,
      '#description' => t('Amount of the product is which available for sale.'),
    );
    $form['product']['inventory']['reserved'] = array(
      '#type' => 'textfield',
      '#title' => t('Items reserved'),
      '#default_value' => ((isset($node->reserved) && $node->reserved != '') ? $node->reserved : 0),
      '#size' => 25,
      '#maxlength' => 50,
      '#description' => t('Amount of the product which has been sold, but has not been shipped or the order has not been completed'),
    );
  }
}

/**
 * Implementation of hook_feature_info().
 */
function ec_availability_feature_info() {
  return array(
    'availability' => array(
      'name' => t('Availability'),
      'description' => t('Track availabilty of products. These products can be inventory based or restricted like an event or training session.'),
      'module' => 'ec_availability',
    ),
  );
}

/**
 * Implementation of hook_feature_load().
 */
function ec_availability_load($node) {
  if (is_object($node)) {
    $nid = $node->nid;
  }
  elseif (is_array($node)) {
    $nid = $node['nid'];
  }
  else {
    $nid = $node;
  }

  if ($extra = db_fetch_array(db_query('SELECT * FROM {ec_available} WHERE nid = %d', $nid))) {
    return $extra;
  }
}

/**
 * Implementation of hook_feature_insert().
 */
function ec_availability_insert($node) {
  drupal_write_record('ec_available', $node);
}

/**
 * Implementation of hook_feature_update().
 */
function ec_availability_update($node) {
  drupal_write_record('ec_available', $node, 'nid');
  if (!db_affected_rows()) {
    drupal_write_record('ec_available', $node);
  }
}

/**
 * Implementation of hook_feature_delete().
 */
function ec_availability_delete($node) {
  db_query('DELETE FROM {ec_available} WHERE nid = %d', $node->nid);
}

/**
 * Implementation of hook_feature_attributes().
 */
function ec_availability_attributes($node) {
  if (!isset($node->available) || $node->available <= 0) {
    return array('no_stock' => TRUE);
  }
}

/**
 * Implementation of hook_feature_ec_checkout_validate_item().
 *
 * FIXME: This check needs to be changed to always be done from the db, not
 * the node past.
 */
function ec_availability_ec_checkout_validate_item($node, $type, $qty, $data, $severity, $return) {
  $qty = $qty ? $qty : 1;
  $alloc = ec_availability_get_current($node->nid);
  if ($qty > $alloc['current']) {
    if ($severity > EC_VALIDATE_ITEM_SEVERITY_MEDIUM) {
      form_set_error("products][{$node->nid}][qty", t('Not enough %title available to fulfill the amount of %qty', array('%title' => $node->title, '%qty' => format_plural($qty, '1 item', '@count items'))));
    }
    return FALSE;
  }
}

/**
 * Implementation of hook_feature_ec_checkout_add_complete().
 */
function ec_availability_ec_checkout_add_complete($node, $qty, $data) {
  $expires_period = 300;

  db_query("UPDATE {ec_available_alloc} SET nid = %d, cookie_id = '%s', expires = %d, allocated = %d", $node->nid, ec_checkout_get_id(), time()+$expires_period, $qty);

  if (!db_affected_rows()) {
    db_query("INSERT INTO {ec_available_alloc} (nid, cookie_id, expires, allocated) VALUES (%d, '%s', %d, %d)", $node->nid, ec_checkout_get_id(), time()+$expires_period, $qty);
  }
}

function ec_availability_cart_remove_item($node, $cookie_id) {
  db_query("DELETE FROM {ec_available_alloc} WHERE nid = %d AND cookie_id = '%s'", $node->nid, $cookie_id);
}

function ec_availability_transaction($node, $op) {
  if ($op == 'load') {
    return ec_availability_load($node);
  }
}

function ec_availability_txn_workflow($node, $txn, $orig) {
  /**
   * Remove the quantity sold when the transaction is created. Not once the
   * transaction has been completed.
   */
  if (empty($orig)) {
    db_query('UPDATE {ec_available} SET available = %d WHERE nid = %d', $node->available-$node->qty, $node->nid);
  }

  $workflow = ec_store_transaction_workflow('type', $txn->workflow);
  $orig_workflow = ec_store_transaction_workflow('type', $orig->workflow);
  switch ($workflow['type']) {
    case EC_WORKFLOW_TYPE_COMPLETE:
      if (!empty($orig) && $orig_workflow['type'] != EC_WORKFLOW_TYPE_COMPLETE) {
        db_query('UPDATE {ec_available} SET reserved = %d WHERE nid = %d', $node->reserved-$node->qty, $node->nid);
      }
      break;

    case EC_WORKFLOW_TYPE_CANCEL:
      if (!empty($orig) && $orig_workflow['type'] != EC_WORKFLOW_TYPE_CANCEL) {
        db_query('UPDATE {ec_available} SET reserved = %d WHERE nid = %d', $node->reserved-$node->qty, $node->nid);
      }
      if (!empty($orig)) {
        db_query('UPDATE {ec_available} SET available = %d WHERE nid = %d', $node->available+$node->qty, $node->nid);
      }
      break;

    default:
      if (empty($orig) || $orig_workflow['type'] != EC_WORKFLOW_TYPE_IN_PROGRESS) {
        db_query('UPDATE {ec_available} SET reserved = %d WHERE nid = %d', $node->reserved+$node->qty, $node->nid);
      }
      break;
  }
}

/**
 * Get the current availability figures
 */
function ec_availability_get_current($nid) {
  $avaiable =& ctools_static(__FUNCTION__ . '_avaiable', array());

  if (empty($avaiable[$nid])) {
    if ($avail = ec_availability_load($nid)) {
      $avail['temp'] = db_result(db_query("SELECT SUM(allocated) FROM {ec_available_alloc} WHERE nid = %d AND cookie_id <> '%s'", $nid, ec_checkout_get_id()))+0;
      $avail['current'] = $avail['available']-$avail['temp'];
      $avail['actual'] = $avail['available']+$avail['reserved'];
      $avaiable[$nid] = $avail;
    }
  }

  return empty($avaiable[$nid]) ? array('available' => 0) : $avaiable[$nid];
}

