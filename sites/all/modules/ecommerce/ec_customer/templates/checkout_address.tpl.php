<?php

/**
 * @file
 */

$output = '';
if (isset($form['use_for_shipping'])) {
  $output .= drupal_render($form['use_for_shipping']);
}

$output .= drupal_render($form['select_address']) . drupal_render($form['address_submit']);
?>
<div id="<?php echo $form['#address_type']; ?>-address-form" class="address-form">
  <?php echo drupal_render($form); ?>
</div>
<?php echo $output; ?>
