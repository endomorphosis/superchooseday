<?php

/**
 * @file
 * Install and uninstall routines, incremental database updates and database
 * schema.
 */

/**
 * e-Commerce paypal module schema
 */
function ec_paypal_install() {
  $ret = array();

  db_add_field($ret, 'ec_receipt', 'paypal_payer_email', array('type' => 'varchar', 'length' => '127', 'not null' => TRUE, 'default' => ''), array('indexes' => array('paypal_payer_email' => array('paypal_payer_email'))));
  db_add_field($ret, 'ec_receipt', 'paypal_subscr_id', array('type' => 'varchar', 'length' => '127', 'not null' => TRUE, 'default' => ''));
}

/**
 * Implementation of hook_uninstall().
 */
function ec_paypal_uninstall() {
  $ret = array();

  db_drop_index($ret, 'ec_receipt', 'paypal_payer_email');
  db_drop_field($ret, 'ec_receipt', 'paypal_payer_email');
  db_drop_field($ret, 'ec_receipt', 'paypal_subscr_id');

  foreach (array('ec_paypal_receiver_email', 'ec_paypal_url', 'ec_paypal_pdt_token', 'ec_paypal_api_url', 'ec_paypal_api_username', 'ec_paypal_api_password', 'ec_paypal_api_signature', 'ec_paypal_custom_page_style', 'ec_paypal_return_url', 'ec_paypal_cancel_url', 'ec_paypal_ipnrb_url', 'ec_paypal_currency_code', 'ec_paypal_cart_description') as $key) {
    variable_del($key);
    variable_del(preg_replace('/^ec_paypal_/i', 'ec_paypal_sandbox_', $key));
  }
  variable_del('ec_paypal_sandbox_enabled');
}

function ec_paypal_schema_alter(&$schema) {
  $schema['ec_receipt']['fields']+= array(
    'paypal_payer_email' => array('type' => 'varchar', 'length' => '127', 'not null' => TRUE),
    'paypal_subscr_id' => array('type' => 'varchar', 'length' => '127', 'not null' => TRUE, 'default' => ''),
  );
  $schema['ec_receipt']['indexes']['paypal_payer_email'] = array('paypal_payer_email');
}


/**
 * Create ec_receipt_paypal for conversion
 */
function ec_paypal_update_2() {
  $ret = array();
  $version = drupal_get_installed_schema_version('ec_anon', TRUE);
  
  if ($version < 6400) {
    $ret['#abort'] = array('success' => FALSE, 'query' => t('Waiting until eC Anonymous has completed the initial upgrade'));
    return $ret;
  }
  
  $version = drupal_get_installed_schema_version('ec_receipt', TRUE);
  
  if ($version < 6400) {
    $ret['#abort'] = array('success' => FALSE, 'query' => t('Waiting until eC Receipt has completed the initial upgrade'));
    return $ret;
  }
  
  db_create_table($ret, 'ec_receipt_paypal', array(
    'description' => 'eCommerce Receipts Paypal',
    'fields' => array(
      'erid' => array(
        'description' => t('Receipt Id'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'txn_id' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'payer_email' => array(
        'type' => 'varchar',
        'length' => 127,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('erid'),
  ));
  
  return $ret;
}

/**
 * Convert all the payer_emails to a md5 hash as we don't use this value
 * anywhere and it adds an extra layer of privacy.
 */
function ec_paypal_update_3() {
  $ret[] = update_sql("UPDATE {ec_receipt_paypal} SET payer_email = MD5(payer_email) WHERE payer_email <> ''");
 
  return $ret;
}

/**
 * Add subscr_id field to store the subscription id
 */
function ec_paypal_update_6401() {
  $ret = array();

  db_add_field($ret, 'ec_receipt', 'paypal_payer_email', array('type' => 'varchar', 'length' => '127', 'not null' => TRUE, 'default' => ''), array('indexes' => array('paypal_payer_email' => array('paypal_payer_email'))));
  db_add_field($ret, 'ec_receipt', 'paypal_subscr_id', array('type' => 'varchar', 'length' => '127', 'not null' => TRUE, 'default' => ''));

  return $ret;
}

/**
 * Copy all the data from ec_receipt_paypal to ec_receipt
 */
function ec_paypal_update_6402() {
  $ret = array();

  if (!db_column_exists('ec_receipt', 'extern_id')) {
    $ret['#abort'] = 1;
    return $ret;
  }

  $ret[] = update_sql('UPDATE {ec_receipt} r SET r.extern_id = (SELECT p.txn_id FROM {ec_receipt_paypal} p WHERE p.erid = r.erid), r.paypal_payer_email = (SELECT p.payer_email FROM {ec_receipt_paypal} p WHERE p.erid = r.erid)');

  return $ret;
}

/**
 * Drop the ec_receipt_paypal table
 */
function ec_paypal_update_6403() {
  $ret = array();

  db_drop_table($ret, 'ec_receipt_paypal');

  return $ret;
}