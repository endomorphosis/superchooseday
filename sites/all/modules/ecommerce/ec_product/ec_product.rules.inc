<?php

/**
 * @file
 * Rules implementation for ec_product module.
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_product_rules_event_info() {
  return array(
    'transaction_product_save' => array(
      'label' => t('Transaction Product Save'),
      'module' => 'eC Product',
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Altered Transaction')),
        'original' => array('type' => 'transaction', 'label' => t('Original Transaction')),
        'item' => array('type' => 'node', 'label' => t('Transaction Item')),
      ),
    ),
  );
}

