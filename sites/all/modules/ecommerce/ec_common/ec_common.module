<?php

/**
 * @file
 * This module provides a general shared library for e-Commerce. It also
 * manages a number of generic callbacks, like gathering permissions.
 */

define('EC_PERM_REPORT', 'store admin report');
define('EC_PERM_MANAGE', 'store admin manage');
define('EC_PERM_SETTINGS', 'store admin settings');

/**
 * Implementation of hook_perm().
 * This is the main permissions controller for e-Commerce.
 * The main three permissions for e-Commerce are:
 * - 'store admin settings'
 *      For configuring the various modules. Setting up
 *      product types/features, receipt types, etc.
 * - 'store admin report'
 *      This allows read-only viewing of store information, but no access
 *      to changing information like transactions.
 * - 'store admin manage'
 *      For the users who run the store, changing transactions, workflows,
 *      creating invoices, and so on.
 *
 * Note: you should assign 'ec perm report' to someone who also has
 * 'ec perm manage'.
 */
function ec_common_perm() {
  $perms = array(EC_PERM_REPORT, EC_PERM_SETTINGS, EC_PERM_MANAGE);
  return array_merge($perms, module_invoke_all('ec_perm'));
}

/**
 * Implementation of hook_menu().
 */
function ec_common_menu() {
  $items = array();

  $items['admin/store'] = array(
    'access callback' => 'user_access',
    'access arguments' => array(EC_PERM_REPORT),
    'page callback' => 'ec_common_settings_overview',
    'page arguments' => array('admin/store'),
    'description' => 'Manage your store',
    'title' => 'e-Commerce',
    'position' => 'right',
    'file' => 'ec_common.admin.inc',
    'options' => array(
      'admin' => TRUE,
    ),
  );
  $items['admin/ecsettings'] = array(
    'access callback' => 'user_access',
    'access arguments' => array(EC_PERM_SETTINGS),
    'page callback' => 'ec_common_settings_overview',
    'page arguments' => array('admin/ecsettings'),
    'description' => 'Configure your e-Commerce store.',
    'title' => 'e-Commerce configuration',
    'file' => 'ec_common.admin.inc',
    'options' => array(
      'admin' => TRUE,
    ),
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function ec_common_theme() {
  return array(
    'format_currency' => array(
      'arguments' => array(
        'price' => NULL,
        'currency' => variable_get('ec_default_currency', 'USD'),
      ),
      'template' => 'format-currency',
      'pattern' => 'format-currency__',
      'path' => drupal_get_path('module', 'ec_common') .'/templates',
      'file' => 'theme.inc',
    ),
    'ec_views_exposed_form' => array(
      'arguments' => array(
        'form' => NULL,
      ),
      'template' => 'ec-views-exposed-form',
      'pattern' => 'ec-views-exposed-form__',
      'path' => drupal_get_path('module', 'ec_common') .'/templates',
      'file' => 'theme.inc',
    ),
    'ec_views_exposed_form_fieldset' => array(
      'arguments' => array(
        'form' => NULL,
      ),
      'template' => 'ec-views-exposed-form-fieldset',
      'pattern' => 'ec-views-exposed-form-fieldset__',
      'path' => drupal_get_path('module', 'ec_common') .'/templates',
      'file' => 'theme.inc',
    ),
    'ec_common_style_plugin_table' => array(
      'arguments' => array('form' => NULL),
      'file' => 'ec_common.admin.inc',
    ),
    'ec_node_selector_multi' => array(
      'arguments' => array('element' => NULL),
      'file' => 'templates/theme.inc',
    ),
    'ec_view_bulk_multi' => array(
      'arguments' => array(
        'header' => array(),
        'header_attributes' => array(),
        'rows' => array(),
        'rows_attributes' => array(),
        'cols_attributes' => array(),
        'attributes' => array(),
        'title' => NULL,
        'view' => NULL
      ),
      'path' => drupal_get_path('module', 'ec_common') .'/templates',
      'file' => 'theme.inc',
      'template' => 'ec-view-bulk-multi',
    ),
  );
}

/**
 * Implementation of hook_elements().
 */
function ec_common_elements() {
  $types = array();
  
  $types['checkbox'] = array('#process' => array('ec_common_process_external_required'));
  $types['checkboxes'] = array('#process' => array('ec_common_process_external_required'));
  $types['date'] = array('#process' => array('ec_common_process_external_required'));
  $types['password'] = array('#process' => array('ec_common_process_external_required'));
  $types['password_confirm'] = array('#process' => array('ec_common_process_external_required'));
  $types['radio'] = array('#process' => array('ec_common_process_external_required'));
  $types['radios'] = array('#process' => array('ec_common_process_external_required'));
  $types['select'] = array('#process' => array('ec_common_process_external_required'));
  $types['textarea'] = array('#process' => array('ec_common_process_external_required'));
  $types['textfield'] = array('#process' => array('ec_common_process_external_required'));
  $types['weight'] = array('#process' => array('ec_common_process_external_required'));
  
  $types['ec_node_selector_multi'] = array(
    '#input' => TRUE,
    '#view' => NULL,
    '#process' => array('views_node_selector_process'),
  );
  
  return $types;
}

/**
 * Implementation of hook_views_api().
 */
function ec_common_views_api() {
  return array('api' => 2.0);
}

/**
 * Sort Array of objects by weight
 */
function ec_sort($a, $b) {
  $a = is_object($a) ? (array)$a : $a;
  $b = is_object($b) ? (array)$b : $b;

  if (isset($a['parent']) && isset($b['parent']) && $a['parent'] != $b['parent']) {
    $a_weight = !empty($a['parent']) ? $a['parent'] : 0;
    $b_weight = !empty($b['parent']) ? $b['parent'] : 0;
  }
  else {
    $a_weight = !empty($a['weight']) ? $a['weight'] : 0;
    $b_weight = !empty($b['weight']) ? $b['weight'] : 0;
  }

  if ($a_weight == $b_weight) {
    return 0;
  }
  return ($a_weight < $b_weight) ? -1 : 1;
}

/**
 * Sort array of objects by weight in reverse
 */
function ec_sort_desc($a, $b) {
  if (is_object($a)) {
    $a_weight = !empty($a->weight) ? $a->weight : 0;
    $b_weight = !empty($b->weight) ? $b->weight : 0;
  }
  else {
    $a_weight = !empty($a['weight']) ? $a['weight'] : 0;
    $b_weight = !empty($b['weight']) ? $b['weight'] : 0;
  }

  if ($a_weight == $b_weight) {
    return 0;
  }
  return ($a_weight < $b_weight) ? 1 : -1;
}

/**
 * Format the price according to payment_settings() config options.
 */
function format_currency($price, $currency = NULL, $call_hook = TRUE) {
  if (!$currency) {
    $currency = variable_get('ec_default_currency', 'USD');
  }

  drupal_alter('ec_outbound_price', $price);

  $price = number_format((float) $price,
    variable_get('payment_decimal_places', 2),
    variable_get('payment_decimal', '.'),
    variable_get('payment_thousands', ','));

  return theme('format_currency', $price, $currency);
}

/**
 * Exclude values an array based on keys and values.
 */
function ec_array_exclude($array, $keys = NULL, $values = NULL) {
  if (!empty($keys) && is_array($keys)) {
    $keys = array_flip($keys);
    $array = array_diff_key($array, $keys);
  }

  if (!empty($values) && is_array($values)) {
    _ec_array_exclude($values, 'init');
    $array = array_filter($array, '_ec_array_exclude');
  }

  return $array;
}

function _ec_array_exclude($filter, $op = NULL) {
  static $excl;
  if ($op) {
    $excl = $filter;
    return;
  }

  return !in_array($filter, $excl);
}

/**
 * Split the customer full name into two: first name and last name thru Regex.
 *
 * @param $name
 *   String. The full name.
 *
 * @return
 *   Array. fname and lname being the first and the last name.
 */
function ec_common_split_name($name) {
  $names = array();
  if (preg_match('/(.*)\s(.*)/i', $name, $match)) {
    $names['fname'] = $match[1];
    $names['lname'] = $match[2];
  }
  return $names;
}

/**
 * Implementation of hook_form_alter().
 */
function ec_common_form_alter(&$form, &$form_state, $form_id) {
  if (substr($form_id, 0, 28) == 'views_bulk_operations_form__' && is_a($form['#plugin'], 'ec_plugin_style_bulk_multi')) {
    $form['objects']['#type'] = 'ec_node_selector_multi';
  }
}

function ec_common_form_views_exposed_form_alter(&$form, &$form_state) {
  $display =& $form_state['display'];
  $view =& $form_state['view'];

  if (!isset($display->display_options['filters']) && isset($view->display['default']->display_options['filters'])) {
    $display =& $view->display['default'];
  }

  if (!empty($display->display_options['filters']) && ($filters = array_filter($display->display_options['filters'], '_ec_filter_exposed_filter'))) {
    foreach ($filters as $id => $filter) {
      $filter+= array(
        'fields' => array(),
      );
      $form[$id] = array(
        '#type' => 'fieldset',
        '#title' => $filter['fieldset_title'],
        '#collapsible' => $filter['fieldset_collapsible'],
        '#collapsed' => $filter['fieldset_collapsed'],
        '#attributes' => array(
          'class' => 'clear-block',
        ),
        '#info' => array(),
        '#theme' => array_map('_ec_map_change_fieldset_theme', $form['#theme']),
      );

      foreach (array_filter($filter['fields']) as $field) {
        $elements = array($display->display_options['filters'][$field]['expose']['identifier']);
        if (isset($form['#info']['filter-'. $field]['operator'])) {
          $elements[] = $form['#info']['filter-'. $field]['operator'];
          $form['#info']['filter-'. $field]['fieldset'] = $id;
        }

        foreach ($elements as $element) {
          if (isset($form[$element])) {
            $form[$id][$element] = $form[$element];
            unset($form[$element]);
          }
        }

        $form[$id]['#info']['filter-'. $field] = $form['#info']['filter-'. $field];
        unset($form['#info']['filter-'. $field]);
      }
    }

    $form['#theme'] = array_map('_ec_map_change_theme', $form['#theme']);
  }
}

function _ec_filter_exposed_filter($a) {
  return $a['field'] == 'ec_exposed_fieldset';
}

function _ec_map_change_theme($a) {
  return 'ec_'. $a;
}

function _ec_map_change_fieldset_theme($a) {
  return 'ec_'. $a .'_fieldset';
}

/**
 * Validate that the value is a number
 */
function valid_number($element, &$form_state) {
  if (!empty($element['#value']) && !is_numeric($element['#value'])) {
    form_error($element, t('@title must be a numeric value', array('@title' => $element['#title'])));
  }
}

/**
 * Validate the entered value is a valid price
 */
function valid_price($element, &$form_state) {
  $price = normalize_price($element['#value']);

  if ($price === FALSE) {
    form_error($element, t('!name field needs to be a numeric value.', array('!name' => $element['title'])));
  }
  else {
    drupal_alter('ec_inbound_price', $price);
    form_set_value($element, $price, $form_state);
  }
}

/**
 * Returns a price normalized for the price database fields in ecommerce. The
 * price is normalized to a float format and that value is returned. If $price
 * contains characters other than a leading payment_symbol, decimal point or
 * thousands separator this function returns FALSE.
 *
 * @param $price String representation of the price.
 * @return Normalized price string (as a float) or FALSE if $price contains
 * invalid characters. If $price is an empty string it is returned without
 * flagging it as an error.
 */
function normalize_price($price) {
  $price = trim($price);

  if (!empty($price)) {
    $symb = variable_get('payment_symbol', '$');
    $symblen = drupal_strlen($symb);
    $symbpos = (variable_get('payment_symbol_position', 1) == 1 ? 0 : -$symblen);
    $pricestart = ($symbpos == 0 ? $symblen : 0);
    $decimal = variable_get('payment_decimal', '.');

    if (drupal_substr($price, $symbpos, $symblen) == $symb) {
      $price = drupal_substr($price, $pricestart, drupal_strlen($price) - $symblen);
    }

    $price = str_replace(variable_get('payment_thousands', ','), '', $price);
    $price = str_replace(variable_get('payment_decimal', '.'), '.', $price);

    if (is_numeric($price)) {
      return $price;
    }
  }
  else {
    return '';
  }

  return FALSE;
}

/**
 * Build arguments for t() to do the replacements
 */
function ec_build_arguments($values, $prefix = '@') {
  $arguments = array();

  if (!empty($values)) {
    ec_map_prefix($prefix, TRUE);
    $keys = array_map('ec_map_prefix', array_keys($values));
    $arguments = array_combine($keys, $values);
  }

  return $arguments;
}

function ec_map_prefix($a, $init = FALSE) {
  static $prefix;

  if ($init) {
    $prefix = $a;
    return;
  }

  return $prefix . $a;
}

function ec_common_process_external_required($element) {
  if (isset($element['#required_external']) && $element['#required_external']) {
    if (isset($element['#pre_render'])) {
      $element['#pre_render'][] = 'ec_common_pre_render_required_external';
    }
    else {
      $element['#pre_render'] = array('ec_common_pre_render_required_external');
    }
    if (isset($element['#post_render'])) {
      $element['#post_render'][] = 'ec_common_post_render_required_external';
    }
    else {
      $element['#post_render'] = array('ec_common_post_render_required_external');
    }
  }
  return $element;
}

function ec_common_pre_render_required_external($element) {
  $element['#required'] = TRUE;
  return $element;
}

function ec_common_post_render_required_external($content, &$element) {
  $element['#required'] = FALSE;
  return $content;
}

/**
 * Used in array_map to return an array of all the weights.
 */
function ec_common_get_weights($a) {
  return $a->weight;
}

/**
 * Return the lowest weight in an array.
 */
function ec_reduce_weight_lightest($v, $w) {
  $value = (array)$w;

  if (!isset($v) || $value['weight'] < $v) {
    return $value['weight'];
  }

  return $v;
}

function ec_attributes($attributes = array()) {
  foreach ($attributes as $attribute => &$data) {
    $data = implode(' ', (array) $data);
    $data = $attribute . '="' . check_plain($data) . '"';
  }
  return $attributes ? ' ' . implode(' ', $attributes) : '';
}

