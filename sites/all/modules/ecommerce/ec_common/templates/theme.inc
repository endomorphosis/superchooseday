<?php

/**
 * @file
 * Provide preprocess functions for all the theme templates.
 */

function template_preprocess_format_currency(&$variables) {
  $variables['payment_symbol'] = variable_get('payment_symbol', '$');
  if (!variable_get('payment_symbol_position', 1)) {
    $variables['template_files'][] = 'format-currency-'. $variables['currency'] .'-aft';
    $variables['template_files'][] = 'format-currency-aft';
  }
  $variables['template_files'][] = 'format-currency-'. $variables['currency'];
}

/**
 * Default theme function for all filter forms.
 */
function template_preprocess_ec_views_exposed_form(&$vars) {
  views_add_css('views');
  $form = &$vars['form'];

  // Put all single checkboxes together in the last spot.
  $checkboxes = '';

  if (!empty($form['q'])) {
    $vars['q'] = drupal_render($form['q']);
  }

  $vars['widgets'] = array();
  foreach ($form['#info'] as $id => $info) {
    // Set aside checkboxes.
    if (isset($form[$info['value']]['#type']) && $form[$info['value']]['#type'] == 'checkbox') {
      $checkboxes .= drupal_render($form[$info['value']]);
      continue;
    }
    $widget = new stdClass;
    // set up defaults so that there's always something there.
    $widget->label = $widget->operator = $widget->widget = NULL;

    if (!empty($info['label'])) {
      $widget->label = $info['label'];
    }
    if (!empty($info['operator'])) {
      $widget->operator = drupal_render($form[$info['operator']]);
    }
    $widget->widget = drupal_render($form[$info['value']]);
    $vars['widgets'][$id] = $widget;
  }

  // Wrap up all the checkboxes we set aside into a widget.
  if ($checkboxes) {
    $widget = new stdClass;
    // set up defaults so that there's always something there.
    $widget->label = $widget->operator = $widget->widget = NULL;
    $widget->widget = $checkboxes;
    $vars['widgets']['checkboxes'] = $widget;
  }

  // Don't render these:
  unset($form['form_id']);
  unset($form['form_build_id']);
  unset($form['form_token']);

  // This includes the submit button.
  $vars['button'] = drupal_render($form['submit']);

  $vars['extra'] = drupal_render($form);
}

function template_preprocess_ec_views_exposed_form_fieldset(&$vars) {
  $form = &$vars['form'];

  // Put all single checkboxes together in the last spot.
  $checkboxes = '';

  $vars['widgets'] = array();
  foreach ($form['#info'] as $id => $info) {
    // Set aside checkboxes.
    if (isset($form[$info['value']]['#type']) && $form[$info['value']]['#type'] == 'checkbox') {
      $checkboxes .= drupal_render($form[$info['value']]);
      continue;
    }
    $widget = new stdClass;
    // set up defaults so that there's always something there.
    $widget->label = $widget->operator = $widget->widget = NULL;

    if (!empty($info['label'])) {
      $widget->label = $info['label'];
    }
    if (!empty($info['operator'])) {
      $widget->operator = drupal_render($form[$info['operator']]);
    }
    $widget->widget = drupal_render($form[$info['value']]);
    $vars['widgets'][$id] = $widget;
  }

  // Wrap up all the checkboxes we set aside into a widget.
  if ($checkboxes) {
    $widget = new stdClass;
    // set up defaults so that there's always something there.
    $widget->label = $widget->operator = $widget->widget = NULL;
    $widget->widget = $checkboxes;
    $vars['widgets']['checkboxes'] = $widget;
  }
}

/**
 * Display a view as a table style.
 */
function template_preprocess_ec_view_table_multi(&$vars) {
  $view     = $vars['view'];

  // We need the raw data for this grouping, which is passed in as $vars['rows'].
  // However, the template also needs to use for the rendered fields.  We
  // therefore swap the raw data out to a new variable and reset $vars['rows']
  // so that it can get rebuilt.
  // Store rows so that they may be used by further preprocess functions.
  $result   = $vars['result'] = $vars['rows'];
  $vars['rows'] = array();
  $vars['rows_attributes'] = array();
  $vars['cols_attributes'] = array();

  $options  = $view->style_plugin->options;
  $handler  = $view->style_plugin;

  $fields   = &$view->field;
  $columns  = $handler->sanitize_columns($options['columns'], $fields);

  $active   = !empty($handler->active) ? $handler->active : '';
  $order    = !empty($handler->order) ? $handler->order : 'asc';

  $query    = tablesort_get_querystring();
  if ($query) {
    $query = '&' . $query;
  }

  // Fields must be rendered in order as of Views 2.3, so we will pre-render
  // everything.
  $renders = $handler->render_fields($result);
  
  $lines = array();
  foreach ($columns as $field => $column) {
    $line = $options['info'][$field]['row'];
    if (!isset($lines[$line])) {
      $lines[$line] = array();
    }
    $lines[$line][] = $field;
  }
  ksort($lines);
  
  $vars['lines'] = $lines;

  $first_line = reset(array_keys($lines));
  $last_line = end(array_keys($lines));

  foreach ($lines as $line => $cols) {
    foreach ($cols as $field) {
      $column = $columns[$field];
      // render the header labels
      if ($field == $column && $line == $first_line && empty($fields[$field]->options['exclude'])) {
        $label = check_plain(!empty($fields[$field]) ? $fields[$field]->label() : '');
        if (empty($options['info'][$field]['sortable']) || !$fields[$field]->click_sortable()) {
          $vars['header'][$field] = $label;
        }
        else {
          // @todo -- make this a setting
          $initial = 'asc';

          if ($active == $field && $order == 'asc') {
            $initial = 'desc';
          }

          $title = t('sort by @s', array('@s' => $label));
          if ($active == $field) {
            $label .= theme('tablesort_indicator', $initial);
          }
          $link_options = array(
            'html' => true,
            'attributes' => array('title' => $title),
            'query' => 'order=' . urlencode($field) . '&sort=' . $initial . $query,
          );
          $vars['header'][$field] = l($label, $_GET['q'], $link_options);
        }
      }

      // Create a second variable so we can easily find what fields we have and what the
      // CSS classes should be.
      if (!isset($vars['cols_attributes'][$field])) {
        $vars['cols_attributes'][$field] = array(
          'class' => array(
            'views-field',
            'views-field-' . views_css_safe($field),
          ),
        );
        if ($active == $field) {
          $vars['cols_attributes'][$field]['class'][] = ' active';
        }
        if (!empty($options['info'][$field]['rowspan'])) {
          $rowspan = $options['info'][$field]['rowspan'];
          $vars['cols_attributes'][$field]['rowspan'] = $rowspan;
        }
        if (!empty($options['info'][$field]['colspan'])) {
          $vars['cols_attributes'][$field]['colspan'] = $options['info'][$field]['colspan'];
        }
      }

      // Render each field into its appropriate column.
      foreach ($result as $num => $row) {
        if (!empty($fields[$field]) && empty($fields[$field]->options['exclude'])) {
          $field_output = $renders[$num][$field];

          if (!isset($vars['rows'][$num][$line][$column])) {
            $vars['rows'][$num][$line][$column] = '';
          }

          // Don't bother with separators and stuff if the field does not show up.
          if ($field_output === '') {
            continue;
          }
          
          // Place the field into the column, along with an optional separator.
          if ($vars['rows'][$num][$line][$column] !== '') {
            if (!empty($options['info'][$column]['separator'])) {
              $vars['rows'][$num][$line][$column] .= filter_xss_admin($options['info'][$column]['separator']);
            }
          }

          $vars['rows'][$num][$line][$column] .= $field_output;
        }
      }
    }
  }

  $count = 0;
  foreach ($vars['rows'] as $num => $row) {
    foreach ($row as $line => $attributes) {
      $vars['rows_attributes'][$num][$line]['class'][] = ($count % 2 == 0) ? 'odd' : 'even';
      if ($line == $first_line) {
        $vars['rows_attributes'][$num][$line]['class'][] = 'first-line';
      }
      if ($line == $last_line) {
        $vars['rows_attributes'][$num][$line]['class'][] = 'last-line';
      }
    }
    $count++;
  }

  foreach ($vars['rows_attributes'][0] as $line => $attributes) {
    $vars['rows_attributes'][0][$line]['class'][] = 'views-row-first';
  }
  $last = count($vars['rows_attributes']) - 1;
  foreach ($vars['rows_attributes'][$last] as $line => $attributes) {
    $vars['rows_attributes'][$last][$line]['class'][] = 'views-row-last';
  }
  
  // Remove all the row spans from the header attributes;
  $vars['header_attributes'] = $vars['cols_attributes'];
  foreach ($vars['header_attributes'] as $field) {
    unset($vars['header_attributes']['rowspan']);
  }
  
  // Go through the columns and make sure that none of the rowspans move outside the group of lines that it is in.
  $count = 0;
  foreach ($lines as $line => $fields) {
    foreach ($fields as $field) {
      if (isset($vars['cols_attributes'][$field]['rowspan']) && is_numeric($vars['cols_attributes'][$field]['rowspan']) && $vars['cols_attributes'][$field]['rowspan'] > count($lines)) {
        $vars['cols_attributes'][$field]['rowspan'] += count($lines) - ($vars['cols_attributes'][$field]['rowspan'] + $count);
      }
    }
    $count++;
  }

  $vars['class'] = 'views-table';
  if (!empty($options['sticky'])) {
    drupal_add_js('misc/tableheader.js');
    $vars['class'] .= " sticky-enabled";
  }
  $vars['class'] .= ' cols-'. count($vars['rows']);
}

/**
 * Theme function for views_node_selector element.
 */
function theme_ec_node_selector_multi($element) {
  module_load_include('inc', 'views', 'theme/theme');

  $output = '';
  $view = $element['#view'];
  $sets = $element['#sets'];
  $vars = array(
    'view' => $view,
  );
  // Give each group its own headers row.
  foreach ($sets as $title => $records) {
    $header = array();
    $header_attributes = array();
    $cols_attributes = array();
    $rows_attributes = array();

    // template_preprocess_views_view_table() expects the raw data in 'rows'.
    $vars['rows'] = $records;

    // Render the view as table. Use the hook from from views/theme/theme.inc
    // and allow overrides using the same algorithm as the theme system will
    // do calling the theme() function.
    $hook = 'ec_view_table_multi';
    $hooks = theme_get_registry();
    if (!isset($hooks[$hook])) {
      return '';
    }
    $args = array(&$vars, $hook);
    foreach ($hooks[$hook]['preprocess functions'] as $func) {
      if (function_exists($func)) {
        call_user_func_array($func, $args);
      }
    }
    
    $rows_attributes = $vars['rows_attributes'];
    $cols_attributes = $vars['cols_attributes'];
    $cols_attributes['select'] = array(
      'rowspan' => count($vars['lines']),
      'class' => array('select'),
    );

    // Add checkboxes to the header and the rows.
    if (empty($view->style_plugin->options['hide_selector'])) {
      $header['select'] = theme('select', $element['selector']);
    }
    else {
      $header['select'] = '';
    }
    $header_attributes['select'] = array('class' => array('select'));
    foreach ($vars['header'] as $field => $label) {
      $header[$field] = $label;
      $header_attributes[$field] = $vars['header_attributes'][$field];
    }
    $first_line = reset(array_keys($vars['lines']));
    $rows = array();
    foreach ($vars['rows'] as $num => $row) {
      $rows[$num][$first_line]['select'] =  theme('checkbox', $element['selection'][_views_bulk_operations_hash_object($view->result[$num])]);
      foreach ($row as $line => $fields) {
        foreach ($fields as $field => $content) {
          $rows[$num][$line][$field] = $content;
        }
      }
    }

    $output .= theme('ec_view_bulk_multi', $header, $header_attributes, $rows, $rows_attributes, $cols_attributes, array('class' => $vars['class']), $title, $view);
    $output .= theme('hidden', $element['selectall']);
  }
  return theme('form_element', $element, $output);
}

/**
 * Template preprocessor for theme function 'views_bulk_operations_table'.
 */
function template_preprocess_ec_view_bulk_multi(&$vars, $hook) {
  $view = $vars['view'];

  $options  = $view->style_plugin->options;
  $handler  = $view->style_plugin;

  $fields   = &$view->field;
  $columns  = $handler->sanitize_columns($options['columns'], $fields);

  $active   = !empty($handler->active) ? $handler->active : '';

  foreach ($columns as $field => $column) {
    $vars['fields'][$field] = views_css_safe($field);
    if ($active == $field) {
      $vars['fields'][$field] .= ' active';
    }
  }

  $count = 0;
  foreach ($vars['rows'] as $num => $row) {
    $vars['row_classes'][$num][] = ($count++ % 2 == 0) ? 'odd rowclick' : 'even rowclick';
  }

  $vars['row_classes'][0][] = 'views-row-first';
  $vars['row_classes'][count($vars['row_classes']) - 1][] = 'views-row-last';

  $vars['class'] = 'views-bulk-operations-table';
  if ($view->style_plugin->options['sticky']) {
    drupal_add_js('misc/tableheader.js');
    $vars['class'] .= ' sticky-enabled';
  }
  $vars['class'] .= ' cols-'. count($vars['rows']);
  $vars['class'] .= ' views-table';
}

